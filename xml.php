<?php

$mem = new Memcached();
$mem->addServer("127.0.0.1", 11211) or die("Unable to connect");

$result = $mem->get("rates");
//$mem->delete("key");

if (!$result) {
	echo "No matching key found. Let’s add it!";
  $url = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
  $xml = simplexml_load_file($url);
  $text = "";
  foreach ($xml->Cube->Cube->Cube as $cube ) {
    $text .= "<li>" . $cube->attributes()->currency . " : " . $cube->attributes()->rate . "</li>";
  }


	$mem->set("rates", $text, 24*60*60);
  $result = $mem->get("rates");
}





print "<ul>";
print($result);
print "</ul>";


?>
