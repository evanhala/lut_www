<?php
session_start();
require_once("utils.php");


siteHeader();

siteNavigation();

if ($_GET["p"] === "login") {
  require("login.php");
}
else if ($_GET["p"] === "register") {
  require("register.php");
}
else if ($_GET["p"] === "ajax") {
  require("ajax.php");
}
else if ($_GET["p"] === "xml") {
  require("xml.php");
}




else {
    require("default.php");
}



siteFooter();


?>
